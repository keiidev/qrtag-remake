@extends('layouts.home')
@section('content')
<div class="px-4 py-5 my-5 text-center w-100">
    <h1 class="display-5 fw-bold text-body-emphasis">Du är död</h1>
    <div class="mx-auto">
        <p class="fs-4">Antingen så väntar du på nästa QRTag eller på en återupplivning.</p>
    </div>
</div>
@stop